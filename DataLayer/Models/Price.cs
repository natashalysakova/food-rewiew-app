﻿using Newtonsoft.Json;
using System;

namespace DataLayer.Models
{
    public class Price
    {
        public decimal Value { get; set; }
        public DateTime PurchaseDate { get; set; }

        public Food Food { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
