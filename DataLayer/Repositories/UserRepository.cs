﻿using DataLayer.Interfaces;
using DataLayer.Models;
using System.Linq;

namespace DataLayer.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository() : base()
        {
        }

        public User GetUserByEmail(string email)
        {
            return _context.GetAll<User>().SingleOrDefault(x => x.Email == email);
        }
    }
}
