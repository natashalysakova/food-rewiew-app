﻿using DataLayer.Interfaces;
using Newtonsoft.Json;

namespace DataLayer.Models
{
    public class Food : IModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }

        public Place Place { get; set; }
        public Price[] Prices { get; set; }
        public Rating[] Ratings { get; set; }       

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
