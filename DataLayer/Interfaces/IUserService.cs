﻿namespace DataLayer.Interfaces
{
    public interface IUserService
    {
        bool CanLogin(string email, string password);
    }
}
