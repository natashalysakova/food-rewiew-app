﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DataLayer.Interfaces;
using DataLayer.Models;
using Microsoft.Azure.Documents;
using Microsoft.Azure.Documents.Client;
using Microsoft.Azure.Documents.Linq;
using Newtonsoft.Json;

namespace DataLayer
{
    public class CosmoDBContext
    {
        private const string EndpointUri = "https://food-review-app.documents.azure.com:443/";
        private const string PrimaryKey = "bb7dmLvBeGCzM24lG98hCgOQca3eUUUQjlGyHS4JGXqyHWJ9s20Ip9anG3mNZdeGY44YL8J3UQ7I8PaWq9b6Xw==";
        private const string DatabaseName = "FoodReviewDatabase";

        private readonly string _databaseName;
        private DocumentClient client;
        private List<Type> types;

        public CosmoDBContext() : this(DatabaseName)
        {
        }

        protected CosmoDBContext(string dbName)
        {
            _databaseName = dbName;
            types = new List<Type>();

            client = new DocumentClient(new Uri(EndpointUri), PrimaryKey);
            client.CreateDatabaseIfNotExistsAsync(new Database { Id = _databaseName });
        }

        private void CreateCollection(Type type)
        {
            client.CreateDocumentCollectionIfNotExistsAsync(
                UriFactory.CreateDatabaseUri(_databaseName),
                new DocumentCollection() {
                    Id = type.Name
                });
        }

        private void ValidateType(Type type)
        {
            if (!types.Contains(type))
            {
                if(type.GetInterface(typeof(IModel).Name) != null)
                {
                    types.Add(type);
                    CreateCollection(type);
                }
            }
        }

        public ICollection<T> GetAll<T>() where T : IModel
        {
            ValidateType(typeof(T));

            var query = client.CreateDocumentQuery<T>(
                UriFactory.CreateDocumentCollectionUri(_databaseName, typeof(T).Name),
                new FeedOptions { MaxItemCount = -1 })
                .AsDocumentQuery();

            var results = new List<T>();
            while (query.HasMoreResults)
            {
                var r = query.ExecuteNextAsync<T>();
                r.Wait();
                results.AddRange(r.Result);
            }

            return results;
        }

        public T Get<T>(string id) where T : IModel
        {
            ValidateType(typeof(T));

            var r = this.client.ReadDocumentAsync<T>(UriFactory.CreateDocumentUri(_databaseName, typeof(T).Name, id));
            r.Wait();
            return r.Result.Document;
        }

        public string Create<T>(T item) where T : IModel
        {
            ValidateType(typeof(T));

            item.Id = Guid.NewGuid().ToString();
            client.CreateDocumentAsync(UriFactory.CreateDocumentCollectionUri(_databaseName, typeof(T).Name), item).Wait();
            return item.Id;
        }

        public void Update<T>(T item) where T : IModel
        {
            ValidateType(typeof(T));

            client.ReplaceDocumentAsync(UriFactory.CreateDocumentUri(_databaseName, typeof(T).Name, item.Id), item).Wait();
        }

        public bool Delete<T>(string id)
        {
            ValidateType(typeof(T));

            try
            {
                client.DeleteDocumentAsync(UriFactory.CreateDocumentUri(_databaseName, typeof(T).Name, id)).Wait();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
