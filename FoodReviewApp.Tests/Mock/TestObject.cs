﻿using DataLayer.Interfaces;
using Newtonsoft.Json;

namespace FoodReviewApp.Tests.Mock
{
    internal class TestObject : IModel
    {
        public int Field1 { get; set; }
        public string Field2 { get; set; }
        [JsonProperty(PropertyName = "id")]
        public string Id { get ; set; }
        public InternalTestObject InternalTestObject { get; set; }
        public InternalArrayObject[] InternalArrayObjects { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }

    internal class InternalTestObject
    {
        public string Field3 { get; set; }
        public int Field4 { get; set; }
    }

    internal class InternalArrayObject
    {
        public string Field5 { get; set; }
    }
}
