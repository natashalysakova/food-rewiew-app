﻿using DataLayer.Models.Enums;
using Newtonsoft.Json;

namespace DataLayer.Models
{
    public class Rating
    {
        public Point Value { get; set; }
        public string Comment { get; set; }

        public User User { get; set; }
        public Food Food { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
