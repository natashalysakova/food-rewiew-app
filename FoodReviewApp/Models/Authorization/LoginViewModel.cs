﻿using System.ComponentModel;

namespace FoodReviewApp.Models.Authorization
{
    public class LoginViewModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
