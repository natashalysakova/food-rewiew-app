﻿using DataLayer.Interfaces;
using Newtonsoft.Json;

namespace DataLayer.Models
{
    public class Place : IModel
    {
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
        public string Address { get; set; }

        public Food[] Foods { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
