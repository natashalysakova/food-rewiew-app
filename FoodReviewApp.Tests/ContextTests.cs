using FoodReviewApp.Tests.Mock;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FoodReviewApp.Tests
{
    [TestClass]
    public class ContextTests
    {
        private ContextMock context;

        [TestInitialize]
        public void Init()
        {
            context = new ContextMock();
        }

        [TestCleanup]
        public void Cleanup()
        {
            var items = context.GetAll<TestObject>();
            foreach (var item in items)
            {
                context.Delete<TestObject>(item.Id);
            }
        }

        [TestMethod]
        public void ContextCreateGetTest()
        {
            //Arrange
            TestObject test = new TestObject()
            {
                Field1 = 1,
                Field2 = "field2",
                InternalTestObject = new InternalTestObject()
                {
                    Field3 = "field3",
                    Field4 = 4
                },
                InternalArrayObjects = new InternalArrayObject[]
                {
                    new InternalArrayObject() { Field5 = "field5"}
                }
            };

            //Action
            string id = context.Create(test);

            //Assert
            var result = context.Get<TestObject>(id);

            Assert.AreEqual(1, result.Field1);
            Assert.AreEqual("field2", result.Field2);
            Assert.AreEqual("field3", result.InternalTestObject.Field3);
            Assert.AreEqual(4, result.InternalTestObject.Field4);
            Assert.AreEqual("field5", result.InternalArrayObjects[0].Field5);

        }

        [TestMethod]
        public void ContextGetAllTest()
        {
            //Arrange
            TestObject test = new TestObject()
            {
                Field1 = 1,
                Field2 = "field2",
                InternalTestObject = new InternalTestObject()
                {
                    Field3 = "field3",
                    Field4 = 4
                },
                InternalArrayObjects = new InternalArrayObject[]
                {
                    new InternalArrayObject() { Field5 = "field5"}
                }
            };

            //Action
            context.Create(test);
            context.Create(test);
            context.Create(test);

            //Assert
            var result = context.GetAll<TestObject>();

            Assert.AreEqual(3, result.Count);
        }

        [TestMethod]
        public void ContextUpdateTest()
        {
            //Arrange
            TestObject test = new TestObject()
            {
                Field1 = 1,
                Field2 = "field2",
                InternalTestObject = new InternalTestObject()
                {
                    Field3 = "field3",
                    Field4 = 4
                },
                InternalArrayObjects = new InternalArrayObject[]
                {
                    new InternalArrayObject() { Field5 = "field5"}
                }
            };

            //Action
            context.Create(test);
            test.Field2 = "UPDATE";
            context.Update(test);

            //Assert
            var result = context.Get<TestObject>(test.Id);

            Assert.AreEqual("UPDATE", result.Field2);
        }

        [TestMethod]
        public void ContextDeleteTest()
        {
            //Arrange
            TestObject test = new TestObject()
            {
                Field1 = 1,
                Field2 = "field2",
                InternalTestObject = new InternalTestObject()
                {
                    Field3 = "field3",
                    Field4 = 4
                },
                InternalArrayObjects = new InternalArrayObject[]
                {
                    new InternalArrayObject() { Field5 = "field5"}
                }
            };

            //Action
            context.Create(test);
            context.Delete<TestObject>(test.Id);

            //Assert
            var result = context.GetAll<TestObject>();
            Assert.AreEqual(0, result.Count);
        }


    }
}
