﻿using DataLayer.Models;

namespace DataLayer.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        User GetUserByEmail(string email);
    }
}
