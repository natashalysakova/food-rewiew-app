﻿using DataLayer.Interfaces;
using System.Collections.Generic;

namespace DataLayer.Repositories
{
    public class Repository<T> : IRepository<T> where T : class, IModel, new()
    {
        protected readonly CosmoDBContext _context;

        public ICollection<T> GetAll()
        {
            return _context.GetAll<T>();
        }

        public T Get(string id)
        {
            return _context.Get<T>(id);
        }

        public string Create(T item)
        {
            return _context.Create(item);
        }

        public void Update(T item)
        {
            _context.Update(item);
        }

        public bool Delete(string id)
        {
            return _context.Delete<T>(id);
        }
    }
}
