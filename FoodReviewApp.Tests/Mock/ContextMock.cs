﻿using DataLayer;

namespace FoodReviewApp.Tests.Mock
{
    internal class ContextMock : CosmoDBContext
    {
        public ContextMock() : base("testDb")
        {
        }
    }
}
