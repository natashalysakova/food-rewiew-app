﻿namespace DataLayer.Interfaces
{
    public interface IModel
    {
        string Id { get; set; }
    }
}
