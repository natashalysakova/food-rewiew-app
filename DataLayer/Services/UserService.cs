﻿using DataLayer.Interfaces;

namespace DataLayer.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public bool CanLogin(string email, string password)
        {
            var user = _repository.GetUserByEmail(email);

            return user != null && user.Password == password;
        }
    }
}
