﻿namespace DataLayer.Models.Enums
{
    public enum Point
    {
        Terrible = 1,
        Bad = 2,
        Average = 3,
        Good = 4,
        Excellent = 5
    }
}
