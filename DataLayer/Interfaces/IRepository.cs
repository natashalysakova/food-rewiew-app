﻿using System.Collections.Generic;

namespace DataLayer.Interfaces
{
    public interface IRepository<T>
    {
        ICollection<T> GetAll();
        T Get(string id);
        string Create(T item);
        void Update(T item);
        bool Delete(string id);
    }
}
